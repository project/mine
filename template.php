<?php
function phptemplate_menu_item_link($item, $link_item) {
  global $user, $menu_is_new;
  if ($user->uid && isset($menu_is_new[$link_item['path']]) && !($item['type'] & MENU_IS_LOCAL_TASK) && $item['path'] != $_GET['q']) {
    $item['title'] = check_plain($item['title']) .'<img src="misc/favicon.ico" />';
    $html = TRUE;
  }
  else {
    $html = FALSE;
  }
  $link = l($item['title'], $link_item['path'], array_key_exists('description', $item) ? array('title' => $items['description']) : array(), NULL, NULL, FALSE, $html);
  return $link;
}
